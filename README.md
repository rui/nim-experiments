[![builds.sr.ht status](https://builds.sr.ht/~ruivieira/nim-experiments/commits/main/.build.yml.svg)](https://builds.sr.ht/~ruivieira/nim-experiments/commits/main/.build.yml?)

# nim-experiments

![](docs/nim-experiments.jpg)

- `logseq`, Parse LogSeq tasks
- `fs`, Common filesystem operations.
- `notes`, markdown note management utility
- `secrets`, Parse secrets files in `~/.config`
- `dockerfiles`, Tools to manipulate docker files
- `readme`, README.md generator for this repository.
- `libmsg`, Library for Markdown SSG with Python bindings.

## 🎓 graduated

- `todoist`, A Nim client to the Todoist REST API ([repo](https://github.com/ruivieira/nim-todoist))
- `gitea`, A Nim client to the Gitea REST API ([repo](https://github.com/ruivieira/nim-gitea))

## mailing lists

- Announcements: [https://lists.sr.ht/~ruivieira/nim-announce](https://lists.sr.ht/~ruivieira/nim-announce)
- Discussion: [https://lists.sr.ht/~ruivieira/nim-discuss](https://lists.sr.ht/~ruivieira/nim-discuss)
- Development: [https://lists.sr.ht/~ruivieira/nim-devel](https://lists.sr.ht/~ruivieira/nim-discuss)

Please prefix the subject with `[$PROJECT-NAME]`.
