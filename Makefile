.PHONY: all dockerfiles libmsg logseq pretty readme secrets

NIM_FILES := $(find . -type f -name "*.nim")
all: dockerfiles libmsg logseq pretty readme
dockerfiles:
	nim c --app:lib --out:libs/dockerfiles.so nim_experiments/src/misc/dockerfiles.nim
libmsg:
	nim c --threads:on --app:lib --out:./libs/libmsg.so nim_experiments/src/misc/libmsg.nim
	cp ./libs/libmsg.so ~/Sync/code/python/msg/msg/
logseq:
	nim c --app:lib --out:libs/logseq.so nim_experiments/src/misc/logseq.nim
pretty:
	nimpretty $(find . -type f -name "*.nim")
readme:
	nim c --out:bin/readme nim_experiments/src/readme/readme.nim
secrets:
	nim c --app:lib --out:libs/secrets.so nim_experiments/src/common/secrets.nim