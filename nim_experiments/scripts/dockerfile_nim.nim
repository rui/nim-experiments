import ../src/misc/dockerfiles

var df = newDockerfile()

df.base("fedora:latest")
df.run(@["dnf update -y", "dnf install -y nim"])
echo df.render()