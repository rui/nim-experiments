import ../src/common/secrets
import json
import todoist

when isMainModule:
    let s = secrets.load("todoist")
    let token = s{"token"}.getStr("")
    let client = todoist.newTodoist(token)
    echo client
    let t = client.getActiveTasks()
    echo $t
    let task_id = t[0].id
    echo "Getting task id " & $task_id
    echo client.getTask(task_id)
    let p = client.getAllProjects()
    echo $p
    let id = p[0].id
    echo "Getting project id " & $id
    echo client.getProject(id)
    echo client.getAllLabels()
    echo client.getProjectSections(id)
