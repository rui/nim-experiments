## README.md generator for this repository.

import fs
import argparse
import os
import tables
import mustache
import json

proc entryCmp(x, y: Table[string, string]): int {.noSideEffect.} =
  let lx = len(x["description"])+len(x["name"])
  let ly = len(y["description"])+len(y["name"])
  if lx < ly: -1
  elif lx == ly: 0
  else: 1

when isMainModule:
  var p = newParser:
    option("-r", "--root", help = "Root for file search")
  try:
    var opts = p.parse(commandLineParams())
    if opts.root == "":
      opts.root = "./src"
    let nim_files = fs.findFiles(opts.root, "nim")
    var entries = newSeq[Table[string, string]]()
    for nim_file in nim_files:
      let f = open(nim_file)
      let firstLine = f.readLine()
      f.close()
      if firstLine.startsWith("## "):
        let entry = {"name": splitFile(nim_file).name, "description": firstLine[
            3..len(firstLine)-1]}.toTable
        entries.add(entry)
    echo entries
    var c = newContext()
    entries.sort(entryCmp)
    c["entries"] = entries

    # parse "graduated" projects
    const graduatedStr = staticRead("./templates/graduated.json")
    let graduatedJson = parseJson(graduatedStr)
    var graduated = newSeq[Table[string, string]]()
    for item in graduatedJson:
      graduated.add({"name": item["name"].getStr(), "description": item[
          "description"].getStr(), "repo": item["repo"].getStr()}.toTable)
    c["graduated"] = graduated

    const tmpl = staticRead("./templates/readme.mustache")
    let readme = tmpl.render(c)
    writeFile("../README.md", readme)

  except ShortCircuit as e:
    if e.flag == "argparse_help":
      echo p.help
      quit(1)
  except UsageError as e:
    stderr.writeLine getCurrentExceptionMsg()
    quit(1)
