## Parse secrets files in `~/.config`

import os
import json
import nimpy

# Read a file of secrets (`.credentials.json`) from the appropriate location
proc load*(topic: string, filename: string = "credentials.json"): JsonNode {.exportpy.} =
    let home = getHomeDir()
    let file = joinPath(home, ".config", topic, filename)
    return parseJson(readFile(file))
