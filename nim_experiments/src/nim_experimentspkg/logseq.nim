## Parse LogSeq tasks

import common/fs
import strutils
import nimpy

type Status* = enum
    DONE, TODO, LATER

type LogSeqTask* = object
    status*: Status
    content*: string

proc parseTasks*(path: string): seq[LogSeqTask] {.exportpy.} =
    var tasks = newSeq[LogSeqTask]()
    let files = fs.findFiles(path, "md")
    for file in files:
        for line in lines(file):
            let trimmed = line.strip(leading = true)
            if trimmed.startsWith("- TODO"):
                let task = LogSeqTask(status: TODO, content: trimmed[7..len(trimmed)-1])
                tasks.add(task)
            elif trimmed.startsWith("- DONE"):
                let task = LogSeqTask(status: DONE, content: trimmed[7..len(trimmed)-1])
                tasks.add(task)
            elif trimmed.startsWith("- LATER"):
                let task = LogSeqTask(status: LATER, content: trimmed[8..len(trimmed)-1])
                tasks.add(task)


    return tasks
