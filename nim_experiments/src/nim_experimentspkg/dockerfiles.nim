## Tools to manipulate docker files
import strutils
import sequtils

type Dockerfile* = object
    commands*: seq[string]

proc newDockerfile*(): Dockerfile =
    var dockerfile = Dockerfile(commands: @[])
    return dockerfile

proc base*(dockerfile: var Dockerfile, base: string) =
    dockerfile.commands.add("FROM " & base)

proc run*(dockerfile: var Dockerfile, run: string) =
    dockerfile.commands.add("RUN " & run)

proc run*(dockerfile: var Dockerfile, run: seq[string]) =
    if len(run) > 1:
        var text = "RUN " & run[0] & " && \\\n"
        text &= run[1..len(run)-1]
            .map(proc(x:string):string = "\t" & x)
            .join(" && \\\n")
        dockerfile.commands.add(text)
    else:
        dockerfile.run(run[0])


proc render*(dockerfile: var Dockerfile): string =
    return dockerfile.commands.join("\n")
