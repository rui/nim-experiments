## Common filesystem operations.

import os
import re

proc findFiles*(path: string, extension: string): seq[string] =
    var matches = newSeq[string]()
    for file in walkDirRec path:
        if file.match re(".*\\." & extension):
            matches.add(file)
    return matches

proc replace_file_extension*(path: string, extension: string): string =
    return path.changeFileExt(extension)