## Library for Markdown SSG with Python bindings.

import nimpy
import fs
from os import fileExists
import strutils

proc findMd*(root: string): seq[string] {.exportpy.} =
    ## Find all Markdown files recursively starting from `root`.
    fs.findFiles(root, "md")

proc replace_file_extension*(path: string, extension: string): string {.exportpy.} =
    ## Replace a file's extension (the string representation, not in disk)
    return fs.replace_file_extension(path, extension)

proc has_paired_notebook*(path: string): bool {.exportpy.} =
    ## Check whether a certain Markdown file has a Jupyter notebook with the same base name.
    let notebookPath = replace_file_extension(path, "ipynb")
    return fileExists(notebookPath)

proc sanitize_link*(title: string): string {.exportpy.} =
    ## Convert a title to a sanitised HTML link
    title.toLower().replace(" ", "-").replace("'", "").replace("(", "").replace(
            ")", "")
