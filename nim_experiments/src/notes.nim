## markdown note management utility

import nimpy
from nimpy/py_lib import pyInitLibPath
import termstyle
import strutils
import std/strformat
import os

let NOTES_SOURCE = "~/Downloads/obsidian"
let LIB_PYTHON = getEnv("LIB_PYTHON")
pyInitLibPath(LIB_PYTHON)

let py = pyBuiltinsModule()

proc move_obsidian*() =
    ## Move Obsidian notes to backlog
    let glob = pyImport("glob")
    let run = pyImport("command_runner")
    let markdown_files = glob.glob("$#/*.md" % [NOTES_SOURCE]).to(seq[string])
    if len(markdown_files)>0:
        echo "[" & yellow "Move Obsidian" & "]"
        echo "ℹ️ Found $# files." % [$len(markdown_files)]
        echo "💪 Moving..."
        let result = run.command_runner("mv -v -n $#/*.md ~/notes/pages/backlog" % [NOTES_SOURCE], shell=true, live_output=true)
        if not result[0].to(int)==0:
            echo red "💀 Failed."
            echo result[1]
        else:
            echo green "👍 Done"
        echo "💪 Compressing images..."
        discard run.command_runner("trimage -d $#/images -q" % [NOTES_SOURCE], shell=true, live_output=true)
        echo "💪 Moving images..."
        discard run.command_runner("mv $#/images/* ~/notes/pages/backlog/images" % [NOTES_SOURCE], shell=true, live_output=true)
    else:
        echo yellow "🤔 No markdown files found."

type
  JiraIssue = ref object of RootObj
    id*: string
    project*: string
    summary*: string
    created*: string
    description*: string

proc createJiraIssue*(id : string, summary : string, created : string, description : string) : JiraIssue =
    var project : string
    if id.startsWith("FAI"):
        project = "trustyai"
    else:
        project = id[0..3]
    JiraIssue(id: id, project: project, summary: summary, created : created, description: description)

proc saveIssue*(issue : JiraIssue) =
    let name = "$1 $2" % [issue.id, issue.summary]
    let text = fmt"""---
title: {name}
type: issue
status: open
project: {issue.project}
created: {issue.created}
id: {issue.id}
---
# Summary

{issue.description}

Issue: [{issue.id}](https://issues.redhat.com/browse/{issue.id})
PR: [PR](https://github.com/...)

## Tasks
"""
    writeFile("$1/notes/work/tickets/$2.md" % [getHomeDir(), name], text)

proc fetch_jira*(id : string) =
    ## Fetch JIRA issue by id
    let jira = pyImport("jira")
    let tracker = jira.JIRA("https://issues.redhat.com")
    let issue = tracker.issue(id)
    let description = if issue.fields.description == py.None : "" else: issue.fields.description.to(string)
    let jira_issue = createJiraIssue(id, issue.fields.summary.to(string), issue.fields.created.to(string), description)
    jira_issue.saveIssue()


when isMainModule:
  import cligen
  dispatchMulti([move_obsidian], [fetch_jira])


