# Package

version       = "0.1.0"
author        = "Rui Vieira"
description   = "A place for Nim experiments"
license       = "AGPL-3.0-only"
srcDir        = "src"
installExt    = @["nim"]
bin           = @["notes", "readme", "humble"]


# Dependencies

requires "nim >= 1.4.4"
requires "nimpy"
requires "argparse"
requires "mustache"
requires "todoist"
requires "termstyle"
requires "cligen"

task notes, "Build and install notes util":
    switch("define", "release")
    setCommand "c"