import unittest

import libmsg
test "correct link sanitization":
  let link1 = "This (is!) a test"
  let link2 = "WhaT 'ABOut' Th(is) (one"
  check libmsg.sanitize_link(link1) == "this-is!-a-test"
  check libmsg.sanitize_link(link2) == "what-about-this-one"
