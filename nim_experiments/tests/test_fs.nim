import unittest

import fs
test "correct extension change":
  let filename = "/foo/bar/baz.txt"
  check fs.replace_file_extension(filename, "md") == "/foo/bar/baz.md"
